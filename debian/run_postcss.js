/*
 * Based on https://github.com/postcss/postcss#usage
 *
 * Copyright 2013 Andrey Sitnik <andrey@sitnik.ru>
 * Licensed under MIT
 */

const autoprefixer = require('autoprefixer')
const postcss = require('postcss')
const fs = require('fs')

fs.readFile('dist/css/materialize.css', (err, css) => {
  postcss([autoprefixer])
    .process(css, { from: 'dist/css/materialize.css', to: 'dist/css/materialize.prefixed.css' })
    .then(result => {
      fs.writeFile('dist/css/materialize.prefixed.css', result.css, () => true)
      if (result.map) {
        fs.writeFile('dist/css/materialize.prefixed.css.map', result.map.toString(), () => true)
      }
    })
})
